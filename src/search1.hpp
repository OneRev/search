#include <iostream>
#include <string>
using namespace std;

int binarySearch(auto Data, auto key)
{
	int low = 0, high = Data.size()-1;
	
	//calculate a mid-point
	int mid = (high + low)/2;
	
	//repeat until element found or no more elements 
	while(low <= high)
	{
		if(key == Data[mid])
			return mid; //we found it
		
		//if key smaller than mid-point element
		if(key < Data[mid])
			high = mid -1; //search in left half of list
		else
			low = mid + 1; //otherwise search in right half
			
		mid =(high + low)/2;
	}//end while
	
	return -1;
}//end function
